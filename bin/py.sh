#!/usr/bin/env bash
# bin/compile <build-dir> <cache-dir>

# fail fast
set -e

# debug
# set -x

shopt -s extglob

function error() {
  echo " !     $*" >&2
  exit 1
}

function indent() {
  c='s/^/       /'
  case $(uname) in
    Darwin) sed -l "$c";;
    *)      sed -u "$c";;
  esac
}

function read_var() {
  echo $(head -n 1 $1)
}

# clean up leaking environment
unset GIT_DIR

# parse and derive params
BUILD_DIR=$1
CACHE=$2
CACHE_DIR="$2/vendor"
LP_DIR=`cd $(dirname $0); cd ..; pwd`
BUILDPACK_DIR="$(dirname $(dirname $0))"
VENDOR_DIR="$BUILD_DIR/vendor"
BASH=$(which bash)
WGET=$(which wget)

# fix STACK variable if unset
if [ -z "${STACK}" ];
then
  STACK="cedar-14"
fi

# config


mkdir -p $CACHE_DIR
PLATFORM="lucid"
ARCH="x86_64"
CONDA_HOME="$1/.conda"
CONDA_BIN="$CONDA_HOME/bin"
RUNTIME="$BUILD_DIR/runtime.txt"
unzip $BUILD_DIR/BOOT-INF/classes/config/ads.zip -d $BUILD_DIR
# Get the runtime version and download appropriate Miniconda
if [ -e $RUNTIME ]; then
    PYTHON_VERSION=$(cut -d- -f2 $RUNTIME)
    if [ ${PYTHON_VERSION:0:1} -eq 3 ]; then
        PYTHON_MAJOR_VERSION=3
    else
        PYTHON_MAJOR_VERSION=""
    fi
    MINICONDA_FILE="Miniconda$PYTHON_MAJOR_VERSION-$PYTHON_VERSION-Linux-x86_64.sh"
else
    MINICONDA_FILE="Miniconda-latest-Linux-x86_64.sh"
fi

MINICONDA_URI="http://repo.continuum.io/miniconda/$MINICONDA_FILE"
MINICONDA_CACHE="$CACHE/$MINICONDA_FILE"

PROFILE_PATH="$BUILD_DIR/.profile.d/conda.sh"

echo "-----> Preparing Python Environment..."
if [ ! -e $MINICONDA_CACHE ] ; then
    echo "-----> Downloading Miniconda..."
    if [ ! -d $CACHE ]; then mkdir $CACHE; fi
    $WGET -q -O $MINICONDA_CACHE $MINICONDA_URI
    chmod +x $MINICONDA_CACHE
fi
if [ -e $CONDA_HOME ]; then rm -rf $CONDA_HOME; fi
# Install miniconda
$MINICONDA_CACHE -b -p $CONDA_HOME #&> /dev/null

echo "-----> Installing Dependencies..."
$CONDA_BIN/conda update --yes --quiet conda
$CONDA_BIN/conda install --yes --quiet pip

# Default Conda env is root
CONDA_ENV="root"
# First check if environment.yml exists and if so use with conda-env
if [ -e "$BUILD_DIR/environment.yml" ]; then
    echo "-----> Installing conda environment from environment.yml..."
    $CONDA_BIN/conda env update -n root -f "$BUILD_DIR/environment.yml"
else
    # No conda environment so use traditional requirements files.
    if [ -e "$BUILD_DIR/conda_requirements.txt" ]; then
        echo "-----> Installing conda packages from conda_requirements.txt..."
        $CONDA_BIN/conda install --yes --quiet --file "$BUILD_DIR/conda_requirements.txt"
    fi
    if [ -e "$BUILD_DIR/BOOT-INF/classes/config/requirements.txt" ]; then
        echo "-----> Installing pip packages from requirements.txt..."
        $CONDA_BIN/pip install --quiet -r "$BUILD_DIR/BOOT-INF/classes/config/requirements.txt"
    fi
fi
$CONDA_BIN/conda clean -pt

#
echo "-----> Fixing paths..."
#grep -rlI $BUILD_DIR . | xargs sed -i.bak "s|$BUILD_DIR|/home/vcap/app|g"
#
grep -rlI "$BUILD_DIR" . | xargs -r sed  -i.bak "s|$BUILD_DIR|/home/vcap/app|g"
#grep -rl "$old" /etc | xargs -r sed -i "s/$old/$new/g"

# Add Conda path to profile
mkdir -p $(dirname $PROFILE_PATH)
echo "export PATH=$HOME/app/.conda/bin:\$PATH" >> $PROFILE_PATH





# copy over environment
mkdir -p $BUILD_DIR/.profile.d

# need to copy binaries back so that any
# installed packages are included in the slug